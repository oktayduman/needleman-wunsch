package benzerlik;

import java.util.ArrayList;
import java.util.List;

public class Grup {
	
	private List<Kisi> list;
	private int kazanc;
	
	public Grup() {
		list = new ArrayList<Kisi>();
		kazanc =0;
	}
	public List<Kisi> getList() {
		return list;
	}

	public void setList(List<Kisi> list) {
		this.list = list;
	}
	
	
	public int getKazanc() {
		return kazanc;
	}
	
	public void setKazanc(int kazanc) {
		this.kazanc = kazanc;
	}
	
	public void arttir() {
		kazanc ++;
	}
	public void azalt() {
		kazanc --;
	}

}
