package benzerlik;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import processing.core.PApplet;

public class Main extends PApplet {

	private static  int GAP = -1;
	private static  int MATCH = 0;
	private static  int MIS_MATCH = 0;
	List<Kisi> kisiList;
	List<Grup> grupList;
	
	List<String> yeni1,yeni2;
	
	Grup grup0,grup1;
	

	

	int matrix[][];

	@Override
	public void setup() {
		// TODO Auto-generated method stub
		super.setup();

		// size(50, 60);
		kisiList = new ArrayList<Kisi>();
		grupList = new ArrayList<Grup>();
		yeni1 = new ArrayList<String>();
		yeni2 = new ArrayList<String>();

		String lines[] = loadStrings("/home/oktay/Downloads/asy.txt");
		println("there are " + lines.length + " lines");
		
		MATCH = Integer.parseInt(lines[0]);
		MIS_MATCH = Integer.parseInt(lines[1]);
		GAP = Integer.parseInt(lines[2]);
		for (int i = 3; i < lines.length; i++) {
			// println(lines[i]);

			String[] temp = lines[i].split(" ");

			kisiList.add(new Kisi(Integer.parseInt(temp[0]), Integer.parseInt(temp[1])));
		}

		Grup simdiki_grup = new Grup();
		grupList.add(simdiki_grup);
		for (int i = 0; i < kisiList.size(); i++) {
			if(i>0){
				Kisi onceki = kisiList.get(i - 1);
				Kisi simdiki = kisiList.get(i);
				/*
				 * if(onceki.getY() == simdiki.getY() ){ //println(simdiki); }
				 */
				if (onceki.getX() == simdiki.getX()) {
					simdiki_grup.getList().add(simdiki);
				} else {
					// System.out.println(simdiki_grup.getList().size());
					simdiki_grup = new Grup();
					simdiki_grup.getList().add(simdiki);
					grupList.add(simdiki_grup);
				}
			}else{
				simdiki_grup.getList().add(kisiList.get(0));
			}
			

		}

		/*
		 * 0 2 0 3 0 8 0 23
		 * 
		 * 
		 * 1 9 1 3 1 8 1 23
		 */
		for (int i = 0; i < grupList.size(); i++) {
			println(grupList.get(i).getList().size());
		}
		
		islet(0);
		println();
		println();
		println();
		islet(1);
		println();
		println();
		println();
		//islet(3);
		println();
		println();
		println();
		


	}
	
	public void islet(int x){
		 grup0 = grupList.get(0);
		 grup1 = grupList.get(x);

		matrix = new int[grup0.getList().size()+1][grup1.getList().size()+1];

		for (int i = 1; i <= grup0.getList().size(); i++) {
			for (int j = 1; j <= grup1.getList().size(); j++) {

				if (i == 0) {
					matrix[i][j] =matrix[i][j-1] +GAP;
				}

				else if (j == 0) {
					matrix[i][j] = matrix[i-1][j] +GAP;;
				} else {
					matrix[i][j] = 0;
				}

			}
		}

		for (int i = 1; i < grup0.getList().size(); i++) {
			for (int j = 1; j < grup1.getList().size(); j++) {

				int caprazSkor = matrix[i - 1][j - 1] + karsilastrir(i, j);
				int ustSkor = matrix[i][j - 1] - 1;
				int yanSkor = matrix[i - 1][j] - 1;

				matrix[i][j] = Math.max(Math.max(ustSkor, yanSkor), caprazSkor);

			}
		}
      println("grup o size :"+grup0.getList().size()+" grup 1 size :"+grup1.getList().size());
      println("matris en :"+matrix[0].length+"matris boy :"+matrix.length);
      println("grup 0 :"+grup0.getList().toString());
	
      println("grup 0 :"+grup1.getList().toString());
		for (int i = 0; i < grup0.getList().size(); i++) {
			for (int j = 0; j < grup1.getList().size(); j++) {
				print("  " + matrix[i][j]);
			}
			println();
		}
		
		
		int i = grup0.getList().size()-1;
		int j = grup1.getList().size()-1;
		
		yeni1.clear();
		yeni2.clear();
		grup1.setKazanc(matrix[i][j]);
		while(i>0&& j > 0){
			if(matrix[i][j]==matrix[i-1][j-1]+karsilastrir(i, j)){
				yeni1.add(""+grup0.getList().get(i-1).getY());
				yeni2.add(""+grup1.getList().get(j-1).getY());
				
				i--;
				j--;
				continue;
			}else if (matrix[i][j]==matrix[i][j-1]-1) {
				yeni1.add("_");
				yeni2.add(""+grup1.getList().get(j-1).getY());
				j--;
				continue;
			}else {
				yeni1.add(""+grup0.getList().get(i-1).getY());
				yeni2.add("_");
				i--;
				continue;
			}
		}
		
		
		Collections.reverse(yeni1);
		Collections.reverse(yeni2);
		for(String s:yeni1){
			print(" "+s);
		}
		println();
		for(String s:yeni2){
			print(" "+s);
		}
		
		println("\n Kazanç  "+grup1.getKazanc());
	}
	
	

	private int karsilastrir(int i, int j) {
		if (grup0.getList().get(i).getY()
				==(grup1.getList().get(j).getY())) {
			//grup1.arttir();
			return MATCH;
		} else {
			//grup1.azalt();
			return MIS_MATCH;
		}
	}

}
